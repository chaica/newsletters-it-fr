# Liste des newsletters IT francophones

Une liste de newsletters IT en français. Cette liste est participative. N'hésitez pas à proposer une Merge Request pour ajouter votre newsletter ! Si vous ne savez pas comment faire, contactez-moi directement (contact tout en bas).

### Actualités

* Planet (informations IT/startups et analyses) https://www.getplanet.eu/
* Newsletter du Blog du Modérateur (formulaire en bas de la page principale) https://www.blogdumoderateur.com/

### Communauté Logiciel Libre/Open Source

* Lettre d'information publique de l'April https://www.april.org/fr/lettre-d-information-publique
* Lettre d'information Framasoft https://contact.framasoft.org/fr/newsletter/
* Le Courrier du hacker https://lecourrierduhacker.com/
* La gazette #bluehats https://github.com/DISIC/gazette-bluehats

### Emploi

* Newsletter LinuxJobs.fr http://eepurl.com/b7kQjL

### Freelance

* Plumes With Attitude (rédacteurs freelances) https://plumeswithattitude.substack.com/

### Généraliste IT

* Newsletter de Developpez.com https://www.developpez.com/newsletter/
* Newsletters du Monde Informatique https://www.lemondeinformatique.fr/compte_utilisateur/newsletter.php

### Infonuagique (cloud)

* Newsletter Padok https://www.padok.fr/blog (formulaire en bas à droite)

### Intelligence artificielle

* Veille Data http://newsletter.datasama.com/

### Nocode

* No Code Station https://nocodestation.com/

### Programmation

#### Javascript

* React Hebdo https://www.getrevue.co/profile/sebastien-lorber

#### Python

* Python Astuces https://pythonastuces.com

#### Ruby

* Women On Rails https://womenonrails.substack.com/

### Web

* La Lettre de la qualité Web https://www.opquast.com/lettre-de-qualite-web-newsletter-opquast/

## Licence
[![CC BY-SA 2.0 FR](https://i.creativecommons.org/l/by-sa/2.0/88x31.png)](https://creativecommons.org/licenses/by-sa/2.0/fr/)

## Éditeur
Carl Chenet - [@carl_chenet](https://twitter.com/carl_chenet) or <carl.chenet@mytux.fr>
